"use strict"

let userName = prompt("Pls enter your name");
let userSurname = prompt("Pls enter your surname");
let birthday = prompt("What is your birthday dd.mm.yyyy = ");

function createUser(userName, userSurname) {
    return{
        name : userName,
        surname : userSurname,
        getLogin : function () {
            let loginName = this.name;
            let loginSurname = this.surname;
            return (loginName[0].toLowerCase() + loginSurname.toLowerCase());
        },
        getAge : function () {
            const nowTime = new Date().getFullYear()
            const yearOfBirth = Number(birthday.slice(6,10))
            return (nowTime - yearOfBirth);
        },
        getPassword : function () {
            return (this.name[0].toUpperCase() + this.surname.toLowerCase() + birthday.slice(6,10))
        }
    }
}
const user = createUser(userName, userSurname);
console.log(user);
console.log("This is your login - " + user.getLogin());
console.log("This is your age - " + user.getAge());
console.log("This is your password - " + user.getPassword());
