"use strict"

let buttons = document.querySelectorAll(".btn-wrapper .btn")
document.addEventListener("keydown", changeColor);
function changeColor(event) {
    buttons.forEach(item=>{
        item.classList.remove("active")
        if (item.innerHTML.toUpperCase() === event.key.toUpperCase()){
            item.classList.add("active")
        }
    })
}