"use strict"


const button = document.getElementById("color-theme");

const loginSection = document.getElementsByClassName("way-to-login")[0];

const buttonToCreateAcc =  document.getElementById("create-account-btn");

const topMenuBg = document.getElementsByClassName("menu-top")[0];


button.addEventListener("click", changeColorTheme)


let i = 0;

function changeColorTheme() {

    if (i % 2 == 0){
        localStorage.setItem("topMenu", "rgba(100%, 40%, 150%, 0.3)")
        localStorage.setItem("loginSectionBgColor", "#d6f0ff" )
        localStorage.setItem("buttonToCreateAcc", "blue")

        loginSection.style.backgroundColor = localStorage.getItem("loginSectionBgColor");
        topMenuBg.style.backgroundColor = localStorage.getItem("topMenu")
        buttonToCreateAcc.style.backgroundColor = localStorage.getItem("buttonToCreateAcc");

    }else{

        localStorage.setItem("topMenu", "rgba(0%, 0%, 0%, 0.4)")
        localStorage.setItem("loginSectionBgColor", "white" )
        localStorage.setItem("buttonToCreateAcc", "#3CB878")

        loginSection.style.backgroundColor = localStorage.getItem("loginSectionBgColor");
        topMenuBg.style.backgroundColor = localStorage.getItem("topMenu")
        buttonToCreateAcc.style.backgroundColor = localStorage.getItem("buttonToCreateAcc");

    }
    i++


}


window.onload = function(){

    if (localStorage.getItem("topMenu") == "rgba(100%, 40%, 150%, 0.3)" ){
        i++
    }

    loginSection.style.backgroundColor = localStorage.getItem("loginSectionBgColor");
    topMenuBg.style.backgroundColor = localStorage.getItem("topMenu")
    buttonToCreateAcc.style.backgroundColor = localStorage.getItem("buttonToCreateAcc");
}

