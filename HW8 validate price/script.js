"use strict";

const input = document.getElementById("inputForPrice");
// console.log(`this is a price from user-  ${inputPrice}`);
const createPriceSection = document.getElementById("createPrice");
const wrongPrice = document.createElement("p");
wrongPrice.classList.add("wrongPrice");
wrongPrice.textContent ="PLS enter correct price";
input.addEventListener("focus", createGreenColor);
input.addEventListener("blur", inputLostFocus);


function createGreenColor() {
    input.style.border = "solid green 2px"
    input.after("")
}

function inputLostFocus() {

    input.style.border ="solid #d6d6d6 2px";
    input.style.color = "green";
    const currentPrice = document.getElementById("inputForPrice").value;

    if (isNaN(currentPrice) || currentPrice <= 0 || currentPrice.includes("+")){
        input.style.border = "solid red 2px";
        input.style.color ="red";
        input.after(wrongPrice);
        return currentPrice

    }
    else {
        wrongPrice.remove()
        createGreenColor()
    }
    const spanPrice = document.getElementById("spanPrice");
    spanPrice.innerText = `CurrentPrice: ${currentPrice}`;
    createPriceSection.style.display = "block"

}

const buttonToDeletePrice = document.getElementById("buttonToDeletePrice");

buttonToDeletePrice.addEventListener("click", ()=>{
    createPriceSection.style.display = "none";
    input.value = "";
});





