// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.



"use strict"

let label = document.getElementsByClassName("input-wrapper");

let input = document.getElementsByTagName("input");

let eyes = document.getElementsByTagName("i")

let submit = document.getElementById("btn");

submit.addEventListener("click", comparePasswords);


function getPassword() {

    for (let i = 0; i < label.length; i++) {
        if (eyes[i].className.includes("visible")) {
            input[i].type = "text;"
        } else {
            input[i].type = "password"
        }

        eyes[i].addEventListener("click", () => {

            if (eyes[i].className.includes("visible")) {
                eyes[i].classList.remove("fas", "fa-eye", "icon-password", "visible")
                eyes[i].classList.add("fas", "fa-eye-slash", "icon-password", "hide");
                input[i].type = "password"
            } else {
                eyes[i].classList.remove("fas", "fa-eye-slash", "icon-password", "hide");
                eyes[i].classList.add("fas", "fa-eye", "icon-password", "visible")
                input[i].type = "text"

            }
        });

    }
}


function comparePasswords() {
    let userPassword = input[0].value;
    let samePassword = input[1].value;
    if (userPassword === samePassword){
        alert("Добро пожаловать!!!")
    }
    else{
        alert("Пароли не совавдают")
    }

}


getPassword();
