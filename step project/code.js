"use strict"



//=======================================================TABS SECTION"OUR SERVICE"= ========
const links = document.querySelectorAll(".ul");
const linksContent = document.querySelectorAll(".content")


for (let i = 0; i<links.length; i++){

    links[i].addEventListener("click", ()=>{

        links.forEach(item=>{
            item.classList.remove("active")
        });
        links[i].classList.add("active")

        linksContent.forEach(item=>{
            item.classList.remove('visible')
            item.classList.add('invisible')
        });
        linksContent[i].classList.remove("invisible")
        linksContent[i].classList.add("visible")
    })

}

